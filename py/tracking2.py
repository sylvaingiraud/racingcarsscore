"""  UTILISATION

Le projet utilise la caméra pour détecter le passage des voitures sous le portique en reconnaissant les couleurs. Au démarrage, les voitures doivent être placées dans chacune des zones de parking pour que le programme enregistre chacune des couleurs.

Le portique peut contenir un bandeau de leds RGB contrôllées par l'Arduino. La connexion de l'Arduino n'est pas obligatoire.

La course est contrôlée depuis la page web  qui affiche aussi les scrores.

1) Brancher la Camera de préférence sur une prise USB v3 et en option Arduino sur une autre USB
   Eviter les ombres dans la zone filmée, Si besoin déplacer le portique ou changer les lumières d'ambience.
2) Lancer racingcars.sh : il execute les scripts getstart.sh, getstop.sh et PreventFreeze-bugmdlight.sh qui doivent tourner en permanance
   Lancer le navigateur, l'URL suivante s'ouvre: http://127.0.0.1/page.html
3) Placer les voitures dans les rectangles
2) Lancer la course avec le bouton"START"
3) Appuyer sur la touche "g" si l'écran de positionnement est activé. Sinon la course démarre sans appui.
4) Affichage du tableau des scores. Lancement du compte à rebours.
5) Stopper la course avec le bouton "STOP"

VARIABLES et REGLAGES:
"""
nbauto             = 4           # Nb of cars in the race. max 9 (cause only 9 car positions)
TimeMinLap         = 6500000000  # en ns. Temps avant de compter un prochain tour(en ns)
ShowPositionScreen = True        # Avant la course affiche l'écran de positionnement qui montre où placer les autos. Si False, la course démarre dès l'appui sur Start.

#Car detection tuning:
ThresholdCarDetection = 200  # Nb of pixels to decide car presence
ThresholdCarNone      = 1000 # Not used
HSVRangeApprox        = 25   # Tolérance sur les valeurs HSV pour la détection.

ShowLive  = False        # Durant la course, affichage de l'image
ShowMasks = False        # Affichage des masks (en phase de mise au point, charge la CPU)

########################


import cv2   # pip3 install opencv-python
import urllib.request
import numpy as np
import time
import os
import vlc  # pour le son. peut nécessiter l'install complète de vlc en ligne de commande

def nothing(x):
    pass

"""
##################################### INSTALLATION
* Installer Apache 2, activer les CGI et copier les fichiers web du projet dans /var/www/html et /var/www/cgi-bin. S'assurer que les scripts sont executables.
* Installer pour Python : OpenCV, piserial, vlc.
* Installer le fichier tracking2.py et les fichiers audio. S'assurer que le chemin vers les fichers audio est correct. 

Les scripts getstart.sh, getstop.sh et PreventFreeze-bugmdlight.sh doivent tourner. 
   (le CGI start.sh trace un message en error.log récupéré par getstart.sh)
   (le CGI stop.sh trace un message en error.log récupéré par getstop.sh)

Brancher la Camera de préférence sur une prise USB v3 et Arduino. La caméra peut être connecté en RTSP mais la fiabilité est moins bonne et la connexion plus lente.

Sur la cam, mettre la résolution 640x480. De préférence desactiver la balance des blancs
Lancer la course avec le bouton"START" (le CGI start.sh trace un message en error.log récupéré par getstart.sh)
3) Placer les voitures dans les 4 rectangles du haut (puis taper g si l'écran de positionnement est activé)
4) Affichage du tableau. Lancement du compte à rebours.
5) Stopper la course avec le bouton "STOP" (le CGI stop.sh trace un message en error.log récupéré par getstop.sh)

A FAIRE:

	pare-soleil au cas où
	classer les scores soit par nb de tours, soit par meilleur tour
	Si l'écran de positionnement n'est pas utilisé, ajouter une tempo pour que la caméra se règle avant le prise des couleurs
	En attente de course, afficher des jeux de lumières
	Algo pour optimiser le réglage des couleurs: pour chaque voiture, chercher le réglage HSV qui masque les autres.
       x le portique flash de la couleur au passage
	x ajout de sound
	x déco du portique
	x last lap,	bestlap
	x filtrage de 8 sec entre 2 mesures
	x cell color
	

---------------- Algo :
0) Essaye de se connecter à la camera sur USB02, si nok essaye 1 URL RTPS, si nok prend une image fixe
   Essaye de se connecter à la Lumiere (Arduino) sur USB01 et USB02. Si échec désactive la lumière
   Configure la camera en 640x360
Différentes options à décider en fonction des résultats
1) Régler la lumière, blanc équilibré, mode scene normal, nuageux
2) Réduire pour éviter trop de pixels blancs
3) Régler le HSV de chaque voiture
     En dur, à l'avance: fastidieux et résultat mauvais quand la lumière change
     "Apprendre" les couleurs au départ et utiliser ces réglages HSV pour la détection
4) Décider si white = calibrate_light
5) Sinon régler setWhite(white) ou L x x x 

x) Go

# Réglages de caméra
webcam logitech : 24 cm en 640 x 370
v4l2-ctl -l
cache lumiere
cam à 24 cm

"""

# Stock le PID du process pour éviter un double lancement par start.sh et permet l'arrêt par stop.sh
MYPID=os.getpid()
print("My PID: ", str(MYPID))
with open('trackingpy.pid', 'w') as f:
   f.write(str(MYPID))
 
#################### DEFINE CARS

# Positions des autos pour la detection des couleurs
# A ajuster si la résolution de la caméra change.
# Les rectangles sont affichés dans la vidéo en mode positionnement 
# 9 positions sont définies ci-dessous dans une image 640x480:
carstartpos = np.array([
                  [  88, 87 ],
                  [ 226, 87 ],
                  [ 364, 87 ],
                  [ 502, 87 ],
                  [  88, 283 ],
                  [ 226, 283 ],
                  [ 364, 283 ],
                  [ 432, 283 ],
                  [ 502, 283 ]
                 ])

# LED reglées: rgb.101.161.161 -> old
#    mesures : L 105 154 107    ->  course:  L 185 254 207
#                     l_h, l_s, l_v     u_h, u_s, u_v
# Default values erased by color detection
name = ['rouge', 'bleu', 'vert', 'jaune','bleuclair','rose','rougenoir','marron','blanc']

# A33 au RAM, réglage de la lumière pour équilibrer le blanc:  rgb.108.254.98
# N'est pas utilisé dans le mode apprentissage des couleurs au démarrage
color = np.array([
                  [ [ 173, 106, 199], [ 207, 218, 255]],     #red
                  [ [ 105, 112, 179], [ 143, 255, 255]],     #blue dark
                  [ [  55,  86, 176], [  82, 176, 255]],     #green
                  [ [  29, 204, 199], [  79, 255, 255]],     #yellow
                  [ [  81, 174, 255], [ 123, 253, 255]],     #blue light
                  [ [ 139,  92, 255], [ 168, 152, 255]],     #rose
                  [ [   0, 186, 190], [  23, 255, 225]],     #red band
                  [ [   0, 148, 206], [  21, 224, 255]],     #rouge orange
                  [ [   0,   0, 255], [   0,   0, 255]]      #white
                 ])
print (color)

colorbgr = np.array([
                  [ 0, 0, 0],
                  [ 0, 0, 0],
                  [ 0, 0, 0],
                  [ 0, 0, 0],
                  [ 0, 0, 0],
                  [ 0, 0, 0],
                  [ 0, 0, 0],
                  [ 0, 0, 0],
                  [ 0, 0, 0]
                 ])

# ref of the color array for each car
carnumber = np.zeros(nbauto)

# for each car:
pixel = np.zeros(nbauto)      # np pixel counted in the mask
laps = np.zeros(nbauto)       # nb laps done, displayed in scoreboard
lastlaps = np.zeros(nbauto)   # time in sec of last lap, displayed in scoreboard
bestlaps = np.zeros(nbauto)   # time in sec of best lap, displayed in scoreboard
timelastlapadded = np.zeros(nbauto)   # absolute time in nsec of last time a lap was added
timelastpass = np.zeros(nbauto)
racetime = np.zeros(nbauto)
autoin = np.zeros(nbauto)
carordered = np.array([0,1,2,3,4,5,6,7,8])
white=0

################# Camera ON:

# Try USB Webcam
# see /dev/video number
# use of VideoCapture class defined above, and not the default cv2 one: no, cause set methods are lost
cam=cv2.VideoCapture(0)
Video=True
if cam.isOpened():
       # Resolutions: v4l2-ctl -d /dev/video3 --list-formats-ext
       # Options: v4l2-ctl -d /dev/video2 -l
	width=640   # 30 cm wide, camera is 24,5 cm above
	height=360   # 17 cm , max speed = 2.1 km/h = 58cm/sec. 17cm in 0.3 sec !
	cam.set(cv2.CAP_PROP_FRAME_WIDTH, width)
	cam.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
	cam.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
	cam.set(cv2.CAP_PROP_FPS, 30)  # Anyway Camera reduces FPS when light is low
	# return camera

if not cam.isOpened():
	print('Cannot open USB cam on 2. Trying RTSP.')
	#exit(-1)

	#def init_videocapture(width=1280, height=720):
	#cam = cv2.VideoCapture(0, cv2.CAP_V4L2)


	# Try from RTSP URL

	#  rtsp://user:pass@192.168.0.189:554/h264Preview_01_main'
	# RTSP_URL = 'rtsp://192.168.178.18:8080/video/h264'
	RTSP_URL = 'rtsp://192.168.26.66:8080/h264_ulaw.sdp'
	RTSP_URL = 'rtsp://192.168.42.129:8080/h264_ulaw.sdp'
	RTSP_URL = 'rtsp://192.168.50.24:8080/h264_ulaw.sdp'    # A33
	#RTSP_URL = 'rtsp://10.42.0.237:8080/h264_ulaw.sdp'    # Pixel via Ubuntu hotspot
	#RTSP_URL = 'rtsp://192.168.17.43:8080/h264_ulaw.sdp'
	# working with http://ip-webcam.appspot.com/ with params 1280x720, quality 31, FPS 10 (opencv crash with some higher values)
	# Free version : https://play.google.com/store/apps/details?id=com.pas.webcam
	# looks more reliable with tcp instead of udp :
	os.environ['OPENCV_FFMPEG_CAPTURE_OPTIONS'] = 'rtsp_transport;udp'

	cam = cv2.VideoCapture(RTSP_URL, cv2.CAP_FFMPEG)  #cv2.CAP_DSHOW) 

	#https://stackoverflow.com/questions/51722319/skip-frames-and-seek-to-end-of-rtsp-stream-in-opencv
	#cam = cv2.VideoCapture("rtspsrc location=rtsp://192.168.17.43:8080/h264_ulaw.sdp ! decodebin ! videoconvert ! appsink max-buffers=1 drop=true", cv2.CAP_FFMPEG)  #cv2.CAP_DSHOW) 
	# =>
	#[ WARN:0@0.068] global cap.cpp:204 open VIDEOIO(FFMPEG): backend is generally available but can't be used to capture by name
	#Cannot open RTSP stream

	if not cam.isOpened():
	    print('Cannot open RTSP stream. Use still image.')
	    #Default picture
	    frame = cv2.imread('/home/hatlab/Documents/RC/py/A33-ram-lignes.png')
	    Video=False

	# From image
	#url='http://192.168.1.61/cam-lo.jpg'
	##'''cam.bmp / cam-lo.jpg /cam-hi.jpg / cam.mjpeg '''

################ DISPLAY WINDOW
cv2.namedWindow("live transmission", cv2.WINDOW_AUTOSIZE)
#cam.set(cv2.CAP_PROP_BUFFERSIZE, 1)  # attempt to reduce buffer to get only last frame: failed.


############### COMMUNICATION ARDUINO LIGHT
# sudo pip3 install pyserial
import serial
# Python code transmits a byte to Arduino /Microcontroller
Lumiere=True
try:
   SerialObj = serial.Serial('/dev/ttyUSB0') # COMxx  format on window, ttyUSBx format on Linux
except (OSError, serial.SerialException):
   Lumiere=False
   pass
if not Lumiere:
   try:
      Lumiere=True
      SerialObj = serial.Serial('/dev/ttyUSB1') # COMxx  format on window, ttyUSBx format on Linux
   except (OSError, serial.SerialException):
      Lumiere=False
      pass
if Lumiere:
   SerialObj.baudrate = 115200  # set Baud rate
   SerialObj.bytesize = 8   # Number of data bits = 8
   SerialObj.parity   ='N'   # No parity
   SerialObj.stopbits = 1   # Number of Stop bits = 1

def rangeByte(value):
   if value >255:
      return 255
   if value < 0:
      return 0
   return value
   
############ Calibrate White
# cam tuning
# v4l2-ctl -c auto_exposure=1
# v4l2-ctl -c white_balance_automatic=0
# manual RGB settings to get balanced RGB (ie. white) on the camera:
#rgb.64.104.61
#rgb.44.74.31
#rgb.44.64.31
#rgb.24.44.21
#rgb.4.4.1
# input is sum of RGB, max is 255*3

#J3 nuages scene normal. White:
#L 68 164 48
#L 108 254 98


# A33 cam 2 no white balance
def calR(sumRGB):
   return str(int(rangeByte(0.3939 * sumRGB + 4.9381)))
def calG(sumRGB):
   return str(int(rangeByte(0.2204 * sumRGB + 10.8892)))
def calB(sumRGB):
   return str(int(rangeByte(0.3856 * sumRGB - 15.8273)))

"""  
#webcam HP
def calR(sumRGB):
   return str(int(0.2796 * sumRGB + 1.6049))
def calG(sumRGB):
   return str(int(0.4567 * sumRGB + 1.8275))
def calB(sumRGB):
   return str(int(0.2637 * sumRGB - 3.4323))
"""


############ ADJUST LIGHT : reduce number of white pixels

# 1 LED only
def set1led(led, r,g,b):
   SerialObj.write(b'l ')
   s = str(led)
   SerialObj.write(s.encode())
   SerialObj.write(b' ')
   s=str(r)
   SerialObj.write(s.encode())
   SerialObj.write(b' ')
   s=str(g)
   SerialObj.write(s.encode())
   SerialObj.write(b' ')
   s=str(b)
   SerialObj.write(s.encode())
   SerialObj.write(b'\n')

# white is 0 to 3 x 255
def setWhite(w):
   SerialObj.write(b'L ')
   #led=str(int(9-i))
   r=calR(w)
   SerialObj.write(r.encode())
   SerialObj.write(b' ')
   r=calG(w)
   SerialObj.write(r.encode())
   SerialObj.write(b' ')
   r=calB(w)
   SerialObj.write(r.encode())
   SerialObj.write(b'\n')

def calibrate_light():
    # Max light up
    #for i in range(25):
    #   SerialObj.write(b'+')    #transmit 'A' (8bit) to micro/Arduino
    #SerialObj.write(b'L 164 254 154')
    white = 550  # max is 3 x 255
    setWhite(white);
    time.sleep(1.5)

    toolight=1
    n_target_pix=43000  # less white pixel expected. 4 cars RBGY resol 1280x720
    while (toolight):

       check = cam.grab() # try to empty buffer to reduce delay
       check = cam.grab()
       check = cam.grab()
       check = cam.grab()
       check, frame = cam.retrieve()   # decode
               # RTSP may need many tries at 1st connection
       while not check:
	       print('Cannot read frame')
	       time.sleep(5);
	       check = cam.grab()
	       check, frame = cam.retrieve()
           
       #hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
       cv2.imshow("live transmission", frame) 
       # img = cv2.imread('img.png', cv2.IMREAD_GRAYSCALE)
       n_white_pix = np.sum(frame == 255)
       print('Nb white pixels:', n_white_pix, " white: ", white)

       if (n_white_pix > n_target_pix):
          if (n_white_pix - n_target_pix > 10000):
             white = white - 50
          else:
             white = white - 10
          setWhite(white)
          time.sleep(0.1)
       else:
          toolight = 0;
       if (white < 51):
          toolight = 0;
       key=cv2.waitKey(1)
       if key==ord('q'):
       	break
    # Add some light to bypass delay issue and consider cars movement
    return white + 30

############ START RACE : 5 red lights
def countdown():
        # Switch off all lights
        SerialObj.write(b'L 0 0 0')
        time.sleep(1)
        # A faire : ino
        #     modifier le ino pour ne pas modifier les autres leds
        #     change le print vers l et L
        #     supprimer tous les autres cases en chiffre, que de lettres.
        # python:
        #      mesure nb par sec, suprimer toutes les traces ds la course
        #      mesure perte du aux traces
        for i in range(0,5):
           set1led(9-i,255,0,0)
           p = vlc.MediaPlayer("/home/hatlab/Documents/RC/py/start-buzz-950.wav")
           p.play()
           time.sleep(1)
        SerialObj.write(b'L 0 0 0')
        time.sleep(0.5)
        p = vlc.MediaPlayer("/home/hatlab/Documents/RC/py/start-buzz-final.mpga")
        p.play()

        #time.sleep(1)
############ GET THE COLORS

def get_cars_colors():
        global frame
        global HSVRangeApprox
        #img_resp=urllib.request.urlopen(url)
        #imgnp=np.array(bytearray(img_resp.read()),dtype=np.uint8)
        #frame=cv2.imdecode(imgnp,-1)
        #_, frame = cap.read()
        
        #cv2
        if Video:    # Else with will use still image
           check, frame = cam.read()
           # RTSP may need many tries at 1st connection
           while not check:
              print('Cannot read frame')
              time.sleep(2);
              check, frame = cam.read()
        
        #sans cv2
        #frame = cam.read()

        if Video:    # Else with will use still image
           while (True):
              check, frame = cam.read()
              if check:
                 for auto in range(nbauto):
                    # cv2.circle(frame,(carstartpos[auto][0],carstartpos[auto][1]),7,(255,255,255),-1)
                    cv2.rectangle(frame,
                       (carstartpos[auto][0],carstartpos[auto][1]),
                       (carstartpos[auto][0]+50,carstartpos[auto][1]+110),
                       (255,255,255),1)
                 cv2.imshow("Position cars and press g", frame)
                 key=cv2.waitKey(1)
                 if key==ord('g') or not ShowPositionScreen:
                    break
           #time.sleep(0.1)


        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        #l_b = np.array([l_h, l_s, l_v])
        #u_b = np.array([u_h, u_s, u_v])
        
        for auto in range(nbauto):
            #cv2.imshow(maskwindow, mask)
            #cv2.imshow("mask2", mask)
            #cv2.imshow("res", res)
            
            """  Average HSV (nok cause black)
            average_color_row = np.average(hsv[carstartpos[auto][1]:carstartpos[auto][1]+110, carstartpos[auto][0]:carstartpos[auto][0]+50], axis=0)
            average_color = np.average(average_color_row, axis=0)
            print(average_color)

            d_img = np.ones((31, 31, 3), dtype=np.uint8)
            d_img[:, :] = average_color
            d_img_bgr = cv2.cvtColor(d_img, cv2.COLOR_HSV2BGR)
            """

            # Dominant  is better than average
            # https://stackoverflow.com/questions/43111029/how-to-find-the-average-colour-of-an-image-in-python-with-opencv
            average = frame[carstartpos[auto][1]:carstartpos[auto][1]+110, carstartpos[auto][0]:carstartpos[auto][0]+50].mean(axis=0).mean(axis=0)
            pixels = np.float32(frame[carstartpos[auto][1]:carstartpos[auto][1]+110, carstartpos[auto][0]:carstartpos[auto][0]+50].reshape(-1, 3))
            n_colors = 5
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 200, .1)
            flags = cv2.KMEANS_RANDOM_CENTERS
            _, labels, palette = cv2.kmeans(pixels, n_colors, None, criteria, 10, flags)
            _, counts = np.unique(labels, return_counts=True)
            dominant = palette[np.argmax(counts)]

            # Draw the color in 1 pixel
            d_img = np.ones((1, 1, 3), dtype=np.uint8)
            d_img[:, :] = dominant
            #d_img_bgr = cv2.cvtColor(d_img, cv2.COLOR_HSV2BGR)
            
            # Keep BGR for display in the scoreboard
            colorbgr[auto] = dominant
            
            # !!!! Problem avec BGR = 255 255 252 => la conversion HSV est nok (H=180)
            # tentative de correction:
            if d_img[0][0][0]>251 and d_img[0][0][1]>251 and d_img[0][0][2]>251:
               d_img[0][0][0]=255
               d_img[0][0][1]=255    #rangeByte(d_img[0][0][1])
               d_img[0][0][2]=255    #rangeByte(d_img[0][0][2])
            
            d_img_hsv = cv2.cvtColor(d_img, cv2.COLOR_BGR2HSV)
            #print (str(auto), " HSV:", d_img_hsv[0], " BGR:", dominant)
            print (str(auto), " HSV:", d_img_hsv[0][0], " BGR:", dominant)
            
            approx = HSVRangeApprox
            color[auto] = [[rangeByte(d_img_hsv[0][0][0]-approx),rangeByte(d_img_hsv[0][0][1]-approx),rangeByte(d_img_hsv[0][0][2]-approx)],
                           [rangeByte(d_img_hsv[0][0][0]+approx),rangeByte(d_img_hsv[0][0][1]+approx),rangeByte(d_img_hsv[0][0][2]+approx)]]
            #color[auto] = [[0,rangeByte(d_img_hsv[0][0][1]-approx),rangeByte(d_img_hsv[0][0][2]-approx)],
            #               [0,rangeByte(d_img_hsv[0][0][1]+approx),rangeByte(d_img_hsv[0][0][2]+approx)]]
            
            #cv2.imshow("DominantColor"+str(auto), d_img)

        print (color)
        print (colorbgr)
        cv2.destroyAllWindows()

############ RUN THE RACE


def run_race():
    print("        LAPS           |              PIXEL  ")
    print(" r  |  b |  g  |  y    |    r   |   b   |   g   |   y")
    print("----------------------------------------------------- ")
    global frame
    global ShowLive
    global ShowMasks

    starttime = time.monotonic_ns()
    for auto in range(nbauto):
      timelastpass[auto]=starttime -5000000000
    nbloops = 0
    nbloopssec = 0
    #print ("starttime:" , str(starttime))

    while True:
        #img_resp=urllib.request.urlopen(url)
        #imgnp=np.array(bytearray(img_resp.read()),dtype=np.uint8)
        #frame=cv2.imdecode(imgnp,-1)
        #_, frame = cap.read()
        
        #cv2
        if Video:    # Else with will use still image
           check, frame = cam.read()
           # RTSP may need many tries at 1st connection
           while not check:
              print('Cannot read frame')
              time.sleep(5);
              check, frame = cam.read()
        
        #sans cv2
        #frame = cam.read()

        if ShowLive:
           cv2.imshow("live transmission", frame)

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        # define the alpha and beta
        #alpha = 1.0 # Contrast control
        #beta = 0.5 # Brightness control
        # call convertScaleAbs function
        #adjusted = cv2.convertScaleAbs(frame, alpha=alpha, beta=beta)
        #cv2.imshow('adjusted', adjusted) 

        #l_b = np.array([l_h, l_s, l_v])
        #u_b = np.array([u_h, u_s, u_v])
        
        for auto in range(nbauto):
            l_b = color[auto][0]
            u_b = color[auto][1]
            mask = cv2.inRange(hsv, l_b, u_b)

            res = cv2.bitwise_and(frame, frame, mask=mask)
            
            """
            # Contour detection
            cnts, _ = cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            #print (cnts)
            for c in cnts:
                area=cv2.contourArea(c)
                if area>300:
                   #cv2.drawContours(res,[c],-1,(255,0,0),3)
                   M=cv2.moments(c)
                   cx=int(M["m10"]/M["m00"])
                   cy=int(M["m01"]/M["m00"])
             
                   cv2.circle(res,(cx,cy),7,(255,255,255),-1)
                   cv2.putText(res,name[auto],(cx-20, cy-20),cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),2)
            """
             
            lastpixel=pixel[auto]
            pixel[auto] = np.count_nonzero(mask)
            # Filtre à affiner. NOK si auto lente. N'est plus utilisé.
            #if pixel[auto] > 1000:
            #   autoin[auto] = 1
            # removed: autoin[auto] == 1 and 
            # filter 8 seconds between to lap count
            if pixel[auto] > ThresholdCarDetection and ((timelastpass[auto] + TimeMinLap) < time.monotonic_ns() ) :
               # 1 new lap detected
               if Lumiere:
                  set1led(9-auto,colorbgr[auto][2],colorbgr[auto][1],colorbgr[auto][0])
               laps[auto]+= 1
               lastlaps[auto] = (time.monotonic_ns() - timelastpass[auto])/1000000000
               if (lastlaps[auto] < bestlaps[auto]) or (bestlaps[auto] == 0) :
                  bestlaps[auto] = lastlaps[auto] 
               timelastpass[auto] = time.monotonic_ns()
               autoin[auto] = 1
            else:
               if Lumiere and autoin[auto] == 1:
                 set1led(9-auto,0,0,0)
               autoin[auto] = 0

            maskwindow="mask"+str(auto)
            if ShowMasks:
               cv2.imshow(maskwindow, mask)
            #cv2.imshow("mask2", mask)
            #cv2.imshow("res", res)

        # How many loops per second ?
        nbloops += 1
        if (starttime + 1000000000 < time.monotonic_ns()):
           #print(str(nbloops), " , " , time.monotonic_ns() - starttime)
           nbloopssec = nbloops
           nbloops = 0
           starttime = time.monotonic_ns()
        
        if pixel[0] + pixel[1] + pixel[2] + pixel[3] > 100:
           print(laps[0]," ",laps[1]," ",laps[2]," ",laps[3]," | ", pixel[0]," ",pixel[1]," ",pixel[2]," ",pixel[3], " l=", nbloopssec, end="      \r")
        
        # Output to file    ▪
        # https://developer.mozilla.org/fr/docs/Learn/CSS/Building_blocks/Styling_tables
        with open('/var/www/html/score/score2.html', 'w') as f:
           f.write("""        
<!DOCTYPE html>
<HEAD><TITLE>CLC HATLAB</TITLE>
<link rel="stylesheet" type="text/css" href="style.css">
 <meta charset="ISO-8859-1"> 
</HEAD><body><table>
<thead><tr>
<th scope="col">CAR COLOR</th>
<th scope="col">Laps</th>
<th scope="col">Last Lap</th>
<th scope="col">Best Lap</th>
</tr></thead><tbody>
""")

           """  
           <tr>   
           <th scope="row"><font color="#008000">OOOOO</font></th>
           <td><font color="#008000">2</font></td>
           <td>9</td>
           <td>32</td>
           </tr>
           """
           #f.seek(-len(os.linesep), os.SEEK_END) 
           #f.write("new text at end of last line" + os.linesep)
           # TODO: Order by Lap
           #carlapssorted = sorted(laps, reverse=True)
           #for auto in range(nbauto):
              
           for eachauto in range(nbauto):
              auto = carordered[eachauto]
              carstylecolor = "color:rgb("+str(colorbgr[auto][2])+","+str(colorbgr[auto][1])+","+str(colorbgr[auto][0])+");\">"
              f.write("<tr><th scope=\"row\" style=\"background-"+carstylecolor+"</th>")
              f.write("<td style=\""+carstylecolor+str(int(laps[auto]))+"</td>")
              f.write("<td style=\""+carstylecolor+str(lastlaps[auto])[:4]+"</td>")
              f.write("<td style=\""+carstylecolor+str(bestlaps[auto])[:4]+"</td></tr>")
              #'\n')
           f.write("</tbody></table></body>")   # + os.linesep
           f.close()

        key=cv2.waitKey(1)
        if key==ord('q'):
           break
        #time.sleep(0.1)

       

############# MAIN

# not done yet:
#init_serial()

print('Adjusting light...')
#white = calibrate_light() 
print('Adjust done.')    

print('Get cars colors')  
if Lumiere:
   SerialObj.write(b'L 199 254 123' )

get_cars_colors()

time.sleep(1)
if Lumiere:
   countdown()
time.sleep(3)

# restore calibrated light
white = 620
if Lumiere:
   print('Restore white:', white)
   #setWhite(white)
   #SerialObj.write(b'L 108 198 88')
   # J3
   #SerialObj.write(b'L 108 254 98')
   # A33 bureau
   SerialObj.write(b'L 199 254 123 ' )
   # A33 RAM  rgb.108.254.98
   #SerialObj.write(b'L 108 254 98' )
   time.sleep(0.2)
   set1led(5,0,0,0)
   #SerialObj.write(b'l 5 0 0 0 ' )
   time.sleep(0.2)
   SerialObj.write(b'l 6 0 0 0 ' )
   time.sleep(0.2) 
   SerialObj.write(b'l 7 0 0 0 ' )
   time.sleep(0.2)
   SerialObj.write(b'l 8 0 0 0 ' )
   time.sleep(0.2)
   SerialObj.write(b'l 9 0 0 0 ' )

print('Start')  
run_race() 
    
if Video:
   cam.release()
cv2.destroyAllWindows()
if Lumiere:
   SerialObj.close()      # Close the port

print("Tracking stopped")


"""

####################""  ARCHIVES:

A33 home
color = np.array([
                  [ [ 152,  168,217], [ 194, 255, 255]],     #red
                  [ [  94,  71, 143], [ 129, 172, 255]],     #blue
                  [ [  45,  34, 152], [  93, 115, 225]],     #green
                  [ [   0,  74, 255], [  29, 218, 255]],     #yellow
                  [ [ 154,  75, 167], [ 186, 203, 254]],     #
                  [ [  94,  71, 143], [ 129, 172, 255]],     #
                  [ [  45,  34, 152], [  93, 115, 225]],     #
                  [ [   0,  74, 255], [  29, 218, 255]]      #
                 ])



# 1eres Measure webcam au RAM
color = np.array([
                  [ [ 130,  41,   0], [ 158, 144,  41]],     #rose
                  [ [  83,  41,  81], [ 123, 131, 242]],     #blue
                  [ [  43,  27,  00], [ 113, 104,  35]],     #green
                  [ [   0,   0, 255], [  50,  80, 255]],    #yellow
                  [ [ 154,  75, 167], [ 186, 203, 254]]     #red
                 ])
"""
"""
# webcam HP lab, lumière ajustée sans point pour moins de pixels blancs
                  [ [  83,  41,  81], [ 123, 131, 242]],     #blue
                  [ [  22,  30,  57], [  80, 104, 223]],     #green
                  [ [   0,   0, 255], [  50,  80, 255]],     #yellow
                  [ [  76,  13,  39], [ 107, 210, 185]]      #orange nok = rouge
"""

#l_h, l_s, l_v = 92, 57, 50
#u_h, u_s, u_v = 142, 153, 178
#l_h, l_s, l_v = 57, 71, 102
#u_h, u_s, u_v = 80, 156, 204

"""
# auto rouge
l_h, l_s, l_v =  83, 131, 181
u_h, u_s, u_v = 209, 251, 238

# auto bleu
l_h, l_s, l_v =  76,  13,  39
u_h, u_s, u_v = 107, 210, 185
# auto verte


"""

"""
# surclass de VideoCapture pour vider le buffer. Mais gene certaines fonction. Inutilisé.
########  bufferless VideoCapture, to get better realtime processing
# warning :  cam.read() renvoie frame au lieu de check,frame avec cv2 
import queue, threading
class VideoCapture:
  def __init__(self, name):
    self.cap = cv2.VideoCapture(name)
    self.q = queue.Queue()
    t = threading.Thread(target=self._reader)
    t.daemon = True
    t.start()
  # read frames as soon as they are available, keeping only most recent one
  def _reader(self):
    while True:
      ret, frame = self.cap.read()
      if not ret:
        break
      if not self.q.empty():
        try:
          self.q.get_nowait()   # discard previous (unprocessed) frame
        except queue.Empty:
          pass
      self.q.put(frame)
  def read(self):
    return self.q.get()
  def set(self):
    return self.set()
"""

