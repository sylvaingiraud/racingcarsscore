# RacingCarsScore


##  UTILISATION

Le projet utilise la caméra pour détecter le passage des voitures sous le portique en reconnaissant les couleurs. Au démarrage, les voitures doivent être placées dans chacune des zones de parking pour que le programme enregistre chacune des couleurs.

Le portique peut contenir un bandeau de leds RGB contrôllées par l'Arduino. La connexion de l'Arduino n'est pas obligatoire.

La course est contrôlée depuis la page web  qui affiche aussi les scrores.

1) Brancher la Camera de préférence sur une prise USB v3 et en option Arduino sur une autre USB
   Eviter les ombres dans la zone filmée, Si besoin déplacer le portique ou changer les lumières d'ambience.
2) Lancer racingcars.sh : il execute les scripts getstart.sh, getstop.sh et PreventFreeze-bugmdlight.sh qui doivent tourner en permanance
   Lancer le navigateur, l'URL suivante s'ouvre: http://127.0.0.1/page.html
3) Placer les voitures dans les rectangles
2) Lancer la course avec le bouton"START"
3) Appuyer sur la touche "g" si l'écran de positionnement est activé. Sinon la course démarre sans appui.
4) Affichage du tableau des scores. Lancement du compte à rebours.
5) Stopper la course avec le bouton "STOP"

## VARIABLES et REGLAGES:

```
nbauto             = 4           # Nb of cars in the race. max 9 (cause only 9 car positions)
TimeMinLap         = 6500000000  # en ns. Temps avant de compter un prochain tour(en ns)
ShowPositionScreen = True        # Avant la course affiche l'écran de positionnement qui montre où placer les autos. Si False, la course démarre dès l'appui sur Start.
```

## Car detection tuning:

```
ThresholdCarDetection = 200  # Nb of pixels to decide car presence
ThresholdCarNone      = 1000 # Not used
HSVRangeApprox        = 25   # Tolérance sur les valeurs HSV pour la détection.

ShowLive  = False        # Durant la course, affichage de l'image
ShowMasks = False        # Affichage des masks (en phase de mise au point, charge la CPU)
```

########################
