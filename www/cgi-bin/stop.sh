#!/bin/bash

echo "stop" > stop-racingcars

# CORS is the way to communicate, so lets response to the server first
echo "Content-type: text/html"    # set the data-type we want to use
echo ""    # we dont need more rules, the empty line initiate this.
echo "<!DOCTYPE html>"
echo "<html><head>"
echo "</head><body>"
echo "Stop!"
echo "<script>history.back()</script>"
echo "</body></html>"    # close html
    
exit 0



