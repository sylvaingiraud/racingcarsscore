#include <FastLED.h>

// For Racing Cars leds
// NodeMCU pinout: https://github.com/FastLED/FastLED/wiki/ESP8266-notes

#ifdef ESP8266
#define RGB_PIN           D1             // LED DATA PIN: D1=GPIO5 => 5 for NodeMCU
#else
#define RGB_PIN           6              // LED DATA PIN: 6 for Nano
#endif 

#define RGB_LED_NUM    30            // 10 LEDs [0...9]
#define BRIGHTNESS     255           // brightness range [0..255]
#define CHIP_SET       WS2812B       // types of RGB LEDs
#define COLOR_CODE     GRB           //sequence of colors in data stream

// Define the array of LEDs
//CRGB LEDs[RGB_LED_NUM];
CRGBArray<RGB_LED_NUM> LEDs;

// define 3 byte for the random color
byte  a, b, c;
#define UPDATES_PER_SECOND 100

char iByte = 0;
int lightness = 128;
int lightR = 108;
int lightG = 198;
int lightB = 88;

void Lightness_tune(int light );
void Led(int led, int R, int G, int B);
int tune(int in, int value);
void run_all_nice(void);

void setup() {
  Serial.begin(115200);
  Serial.println("WS2812B LEDs strip Initialize. Commands:");
  Serial.println("L <R> <G> <B> : set all leds with RGB color.");
  Serial.println("l <Pos> <R> <G> <B>  : set all leds with RGB color.");
  Serial.println("+ or - : increase or decrease global lightness.");
  Serial.println("r or g or b : decrease decrease selected color.");
  Serial.println("R or G or B : increase decrease selected color.");
  Serial.println("A : nice Waiting colors. About 20 seconds.");
  FastLED.addLeds<CHIP_SET, RGB_PIN, COLOR_CODE>(LEDs, RGB_LED_NUM);
  randomSeed(analogRead(0));
  FastLED.setBrightness(BRIGHTNESS);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 500);
  FastLED.clear();
  FastLED.show();

  // All on
  Lightness_tune(lightness);
}

void loop() {

  // clock only
  //while (true) {tic_toc_color();}

  // demo only
  //while (true) {run_all_nice();}

  //if (false) {
  if (Serial.available() > 0) {
    // read the incoming byte:

    iByte = Serial.read();
    //iByte = 'Z';

    switch (iByte)
    {
      case 'A':
        run_all_nice();
        break;
      case '-':
        lightR = tune(lightR, -10);
        lightG = tune(lightG, -10);
        lightB = tune(lightB, -10);
        Lightness_tune(lightness);
        //Serial.print("9.lightness--="); Serial.println(lightness);
        break; 
      case '+':
        lightR = tune(lightR, +10);
        lightG = tune(lightG, +10);
        lightB = tune(lightB, +10);
        Lightness_tune(lightness);
        //Serial.print("0.lightness++="); Serial.println(lightness);
        break;     
      case 'r':
        lightR = tune(lightR, -10);
        //Serial.print("r."); Serial.println(lightR);
        Lightness_tune(lightness);
        break;
      case 'R': lightR = tune(lightR, +10);
        //Serial.print("R."); Serial.println(lightR);
        Lightness_tune(lightness);
        break;
      case 'g':
        lightG = tune(lightG, -10);
        //Serial.print("r."); Serial.println(lightR);
        Lightness_tune(lightness);
        break;
      case 'G': lightG = tune(lightG, +10);
        //Serial.print("R."); Serial.println(lightR);
        Lightness_tune(lightness);
        break;
      case 'b':
        lightB = tune(lightB, -10);
        //Serial.print("r."); Serial.println(lightR);
        Lightness_tune(lightness);
        break;
      case 'B': lightB = tune(lightB, +10);
        //Serial.print("R."); Serial.println(lightR);
        Lightness_tune(lightness);
        break;
      case 'L':
        lightR=Serial.parseInt();
        lightG=Serial.parseInt();
        lightB=Serial.parseInt();
        //Serial.print("R."); Serial.println(lightR);
        Lightness_tune(lightness);
        break;
      case 'l':
        int L=Serial.parseInt();
        int R=Serial.parseInt();
        int G=Serial.parseInt();
        int B=Serial.parseInt();
        //Serial.print("R."); Serial.println(lightR);
        Led(L,R,G,B); 
        break;
    }
  }
}

// Lightness tune all leds
void Lightness_tune(int light ) {
  // Red Green Blue
  for (int i = 0; i < RGB_LED_NUM; i++)
    LEDs[i] = CRGB(lightR, lightG, lightB );
  /*
  for (int i = 4; i < RGB_LED_NUM - 4; i++)
     LEDs[i] = CRGB(0, 0, 0 );
  */
  FastLED.show();
  Serial.print("L "); Serial.print(lightR);Serial.print(" ");Serial.print(lightG);Serial.print(" ");Serial.println(lightB);
  //delay(100);
  /*
  for (int i = 0; i < RGB_LED_NUM; i++)
    LEDs[i] = CRGB(0, 0, 0 );
  FastLED.show();
  delay(1000);
  */
}

// Lightness tune 1 led only
void Led(int led, int R, int G, int B) {
  // Red Green Blue
  for (int i = 0; i < RGB_LED_NUM; i++)
    if (i==led) { LEDs[i] = CRGB(R, G, B); }
    // uncomment to dispay only this led 
    // else        { LEDs[i] = CRGB(0, 0, 0); }
  FastLED.show();
  Serial.print("l "); Serial.print(led);Serial.print(" ");Serial.print(R);Serial.print(" ");Serial.print(G);Serial.print(" ");Serial.println(B);
  //delay(100);
  /*
  for (int i = 0; i < RGB_LED_NUM; i++)
    LEDs[i] = CRGB(0, 0, 0 );
  FastLED.show();
  delay(1000);
  */
}

int tune(int in, int value) {
    int out = in + value;
    if (out > 254) { out = 254; }
    if (out < 1) { out = 1; }
    return out;
}

void tic_toc_color_1min(int minute, int hour) {
  // seconds
  for (int sec = 0; sec < 59; sec++)
     {
     // Red Green background
     for (int j = 0; j < RGB_LED_NUM; j++)
       {
       LEDs[j] = CRGB(255 - (j*255/RGB_LED_NUM), (j*255/RGB_LED_NUM), 0 );
       }

     // hours
     LEDs[int(hour*RGB_LED_NUM/23)] = CRGB(200, 0, 200 );
     
     // minutes
     LEDs[int(minute*RGB_LED_NUM/59)] = CRGB(100, 100, 200 );

     // seconds  blue
     LEDs[int(sec*RGB_LED_NUM/59)] = CRGB(0, 0, 255 );
     //if (i > 0) {LEDs[i-1] = CRGB(0, 0, 0 );}
     FastLED.show();
     
     delay(1000);
     }  // seconds
}


void tic_toc_color(void) {
for (int hour = 5; hour < 23; hour++)
 {
 for (int minute = 0; minute < 59; minute++)
  {
  // seconds
  tic_toc_color_1min(minute, hour);
  } // minute
 } // hour
}

// RED LED TOGGLE
void Toggle_RED_LED(void) {
  // Red Green Blue
  for (int i = 0; i < RGB_LED_NUM; i++)
    LEDs[i] = CRGB(255, 0, 0 );
  FastLED.show();
  delay(1000);
  for (int i = 0; i < RGB_LED_NUM; i++)
    LEDs[i] = CRGB(0, 0, 0 );
  FastLED.show();
  delay(1000);

}
// Move the Red LED
void Scrolling_RED_LED(void)
{
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB::Red;
    FastLED.show();
    delay(500);
    LEDs[i] = CRGB::Black;
    FastLED.show();
    delay(500);

  }
}
// Orange/White/Green color green
void O_W_G_scroll() {
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB::Orange;
    delay(50);
    FastLED.show();
  }
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB::Black;
    delay(50);
    FastLED.show();
  }
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB::White;
    delay(50);
    FastLED.show();
  }
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB::Black;
    delay(50);
    FastLED.show();
  }
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB::Green;
    delay(50);
    FastLED.show();
  }
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB::Black;
    delay(50);
    FastLED.show();
  }
}
// Red/Green/Blue color Rotate
void Rotate_color(void) {
  for (int clr = 0; clr < RGB_LED_NUM; clr++) {
    LEDs[clr]     = CRGB::Red;
    LEDs[clr + 1] = CRGB::Green;
    LEDs[clr + 2] = CRGB::Blue;
    FastLED.show();
    delay(100);
    for (int clr = 0; clr < RGB_LED_NUM; clr++) {
      LEDs[clr] = CRGB::Black;
      delay(5);
    }
  }
}
// Blue, Green , Red 
void r_g_b() {
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB ( 0, 0, 255);
    FastLED.show();
    delay(50);
  }
  for (int i = RGB_LED_NUM; i >= 0; i--) {
    LEDs[i] = CRGB ( 0, 255, 0);
    FastLED.show();
    delay(50);
  }
  for (int i = 0; i < RGB_LED_NUM; i++) {
    LEDs[i] = CRGB ( 255, 0, 0);
    FastLED.show();
    delay(50);
  }
  for (int i = RGB_LED_NUM; i >= 0; i--) {
    LEDs[i] = CRGB ( 0, 0, 0);
    FastLED.show();
    delay(50);
}
}
// random color show
void random_color(void) {
  // loop over the NUM_LEDS
  for (int i = 0; i < RGB_LED_NUM; i++) {
    // choose random value for the r/g/b
    a = random(0, 255);
    b = random(0, 255);
    c = random(0, 255);
    // Set the value to the led
    LEDs[i] = CRGB (a, b, c);
    // set the colors set into the physical LED
    FastLED.show();

    // delay 50 millis
    FastLED.delay(200);
  }
}

void run_all_nice(void) {

        Serial.println("Run nice colors.");
        //Toggle_RED_LED();
        //Scrolling_RED_LED();
        O_W_G_scroll();
        Rotate_color();
        r_g_b();
        random_color();
        // tic_toc_color_1min(55, 10);
        Serial.println("End of Run nice colors.");

}